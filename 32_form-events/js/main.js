$(document).ready(function() {
    $('button#btn-hello').on('click', sayHello);

    function sayHello(){
        alert('hello');
    }

    $('input').on('blur', function() {
        console.log('blurred');
        $input = $(this).val();
        if($input === ""){
            console.log('no input');
            $(this).addClass('red');
        }
        else{
            console.log('there\'s input');
            $(this).removeClass('red').addClass('green');
        }
    });
}); // End of document.ready functie.