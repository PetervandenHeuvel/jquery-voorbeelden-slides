$(document).ready(function () {
    // on click on button
    $('#btnAdd').on('click', function () {
        $('ul').append('<li>New List Item</li>');
    });
    // fade out regular list items
        $('ul').on('click', 'li', function () {
        $(this).fadeOut(500);
    });
});