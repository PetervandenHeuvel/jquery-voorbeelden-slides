$(document).ready(function($) {
    
    // Even and Odd: https://api.jquery.com/even-selector/
    $( "tr:even" ).css( "background-color", "#bbf" );

    // :header Selector: https://api.jquery.com/header-selector/
    // Selects all elements that are headers, like h1, h2, h3 and so on.
    $( ":header" ).css({ background: "#ccc", color: "blue" });

    // :eq() Selector: https://api.jquery.com/eq-selector/ (ZERO-BASED!)
    //$( "td:eq( 2 )" ).css( "color", "red" );

    // :first :last :only https://api.jquery.com/first-selector/
    //$( "tr:first" ).css( "font-style", "italic" );
    //$( "tr:last" ).css({ backgroundColor: "yellow", fontWeight: "bolder" });

    // https://api.jquery.com/only-child-selector/
    // Selects all elements that are the only child of their parent.
    //$( "div button:only-child" ).text( "Alone" ).css( "border", "2px blue solid" );

    // https://api.jquery.com/has-selector/
    // $( "div:has(p)" ).addClass( "test" );

    // https://api.jquery.com/contains-selector/
    //$( "td:contains('John')" ).css( "text-decoration", "underline" );

    // https://api.jquery.com/empty-selector/
    //$( "td:empty" ).text( "Was empty!" ).css( "background", "rgb(255,220,200)" );

    // https://api.jquery.com/parent-selector/
    // Select all elements that have at least one child node (either an element or text).
    //$( "td:parent" ).fadeTo( 1500, 0.3 );

    // https://api.jquery.com/hidden-selector/
    // https://api.jquery.com/visible-selector/
    $( "button" ).click(function() {
      $( "div:hidden" ).show( "fast" );
    });


});