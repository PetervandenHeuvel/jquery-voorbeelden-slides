$(document).ready(function() {
console.log( "ready!" );
    // 1 Document.ready
    $( ".banner" ).fadeIn( 3000, function() {
        $('.banner').delay(2000);
            $( this ).fadeOut( "slow", function() {
            // Animation complete.
        });
    });

    // 2 Document.unload
    $(window).focus(function() {
            document.title = 'My Page';
        });

        $(window).blur(function() {
            document.title = 'Please Come Back!';
        });  


}); // End of document.ready functie.